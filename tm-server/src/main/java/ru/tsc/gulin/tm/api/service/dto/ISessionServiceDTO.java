package ru.tsc.gulin.tm.api.service.dto;

import ru.tsc.gulin.tm.dto.model.SessionDTO;

public interface ISessionServiceDTO extends IUserOwnedServiceDTO<SessionDTO> {
}
