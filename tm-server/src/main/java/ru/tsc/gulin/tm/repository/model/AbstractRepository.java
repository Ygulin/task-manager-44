package ru.tsc.gulin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.model.IRepository;
import ru.tsc.gulin.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public void set(@NotNull Collection<M> models) {
        clear();
        models.forEach(this::add);
    }

    @Override
    public void remove(@NotNull M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
