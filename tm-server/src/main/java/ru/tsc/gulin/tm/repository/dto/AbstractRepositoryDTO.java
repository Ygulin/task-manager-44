package ru.tsc.gulin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gulin.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.gulin.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractRepositoryDTO(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void add(@NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public void set(@NotNull Collection<M> models) {
        clear();
        models.forEach(this::add);
    }

    @Override
    public void remove(@NotNull M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
